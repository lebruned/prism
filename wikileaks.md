# Wikileaks

_WikiLeaks_ [wɪkiˈliːks] est une organisation non gouvernementale fondée par Julian Assange en 2006, dont l'objectif est de publier des documents, pour partie confidentiels, ainsi que des analyses politiques et sociales à l'échelle mondiale. Elle n'a aucune affiliation avec Wikimédia. Sa raison d'être est de donner une audience aux lanceurs d'alertes et aux fuites d'information, tout en protégeant ses sources. Plusieurs millions de documents relatifs à des scandales de corruption, d'espionnage et de violations de droits de l'homme concernant des dizaines de pays à travers le monde ont été publiés sur le site internet depuis sa création.

https://fr.wikipedia.org/wiki/WikiLeaks
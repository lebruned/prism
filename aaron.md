# Aaron Schwartz

_Aaron Swartz_, né le 8 novembre 1986 à Chicago et mort le 11 janvier 2013 à New York1, est un informaticien, écrivain, militant politique et hacktiviste américain.

Fervent partisan de la liberté numérique, il consacra sa vie à la défense de la « culture libre », convaincu que l'accès à la connaissance est un moyen d'émancipation et de justice.

https://fr.wikipedia.org/wiki/Aaron_Swartz